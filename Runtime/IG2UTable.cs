﻿
using System.Collections.Generic;

namespace Daily.G2U
{
    public interface IG2UTable<T>
    {
        public void UpdateTable(List<T> list);
    }
    public interface IG2USubAssetName<T>
    {
        public string GetSubAssetName(T item);
    }
}