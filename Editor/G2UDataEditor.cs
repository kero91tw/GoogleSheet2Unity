﻿using UnityEditor;
using UnityEngine;

namespace Daily.G2U
{
    [CustomPropertyDrawer(typeof(G2UData))]
    public class G2UDataEditor : PropertyDrawer
    {
        private const float Height = 20f;
        private bool _isFold = false;
        private bool _setIcon = false;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + (!_isFold ? 20 : Height * 3);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!_setIcon)
            {
               var texture = AssetDatabase.LoadAssetAtPath<Texture2D>("Packages/daily.googlesheet2unity/Editor/Icon.png");
                EditorGUIUtility.SetIconForObject(property.serializedObject.targetObject, texture);
                _setIcon = true;
            }

            if (!(property.serializedObject.targetObject is ScriptableObject))
            {
                EditorGUI.PrefixLabel(position, new GUIContent("G2UTable只支援ScriptableObject物件"));
                return;
            }
            
            var sheetNameProperty = property.FindPropertyRelative("sheetFileID");
            var worksheetNameProperty = property.FindPropertyRelative("worksheetName");
            
            EditorGUI.BeginProperty(position, label, property);
            var button = new Rect(position.x+position.width*.7f, position.y , position.width*.3f, Height);
            if (GUI.Button(button, new GUIContent("打開表單", EditorGUIUtility.IconContent("Profiler.NetworkOperations@2x").image)))
            {
                Application.OpenURL($"https://docs.google.com/spreadsheets/d/{sheetNameProperty.stringValue}");
            }
            
            var foldoutRect = new Rect(position.x, position.y, position.width, Height);
            _isFold = EditorGUI.Foldout(foldoutRect, _isFold, new GUIContent("G2U設定"));



            if (_isFold)
            {
                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 1;

                var sheetNameRect = new Rect(position.x, position.y + 20, position.width, Height);
                var worksheetNameRect = new Rect(position.x, position.y + 40, position.width, Height);

                EditorGUI.PropertyField(sheetNameRect, sheetNameProperty);
                EditorGUI.PropertyField(worksheetNameRect, worksheetNameProperty);

                EditorGUI.indentLevel = indent;
            }

            var buttonRect = new Rect(position.x, position.y + (!_isFold ? 20 : Height * 3), position.width, Height);
            if (GUI.Button(buttonRect, new GUIContent("更新資料", EditorGUIUtility.IconContent("Download-Available@2x").image)))
            {
                DownLoadData(property.serializedObject.targetObject, sheetNameProperty.stringValue, worksheetNameProperty.stringValue);
            }

            EditorGUI.EndProperty();
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }

        public static void Update(ScriptableObject obj)
        {
            var s= obj.GetType().GetFields(System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic);
            foreach (var i in s)
            {
                var t= i.FieldType.Name.Contains("G2UData");
                if(!t) continue;
               
                var ss = i.GetValue(obj); ;
                var filedId = ss.GetType().GetField("sheetFileID", System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.NonPublic);
                var worksheetName = ss.GetType().GetField("worksheetName", System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.NonPublic);

                // Debug.Log(filedId.GetValue(ss));                  
                // Debug.Log(worksheetName.GetValue(ss));
                DownLoadData(obj,filedId.GetValue(ss).ToString(),worksheetName.GetValue(ss).ToString());
            }
        }

        public static void DownLoadData(Object targetAsset, string sheetName, string workSheetName)
        {
            var tableType = targetAsset.GetType().GetInterface("IG2UTable`1");
            if (tableType == null)
            {
                Debug.LogWarning($"{targetAsset.name} 未實作IG2UDataList介面，無法操作。");
                return;
            }
     
            Debug.Log($"開始下載 {targetAsset.name}");
            
            var tableDataType = tableType.GetGenericArguments();

            DailyG2UManager.GetData(sheetName, workSheetName, data =>
            {
                TableUpdateHelper.SetTable(data, tableDataType, targetAsset);
                EditorUtility.SetDirty(targetAsset);
                AssetDatabase.SaveAssets();
                Debug.Log($"下載完成 {targetAsset.name}");
            });
        }


    }
}