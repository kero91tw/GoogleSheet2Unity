using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Daily.G2U
{
    public static class GoogleAuth2Utility
    {
        public static Action<string> CompleteEvent;

        private static HttpListener _httpListener;
        private static bool _isListening = false;
        public static void OpenUrlGoogleAPI()
        {
            Application.OpenURL("https://console.developers.google.com/apis/credentials");
        }

        public static void StartAuth2(string clientID)
        {
            var serverUrl = $"http://127.0.0.1:{DailyG2UManager.Config.PORT}";

            if (_isListening && _httpListener != null)
            {
                _httpListener.Close();
                _isListening = false;
            }

            _httpListener = new HttpListener();
            _httpListener.Prefixes.Add(serverUrl + "/");
            _httpListener.Start(); // 開始監聽
            _isListening = true;
            Listener();

            var request = "https://accounts.google.com/o/oauth2/v2/auth?";
            request += "client_id=" + Uri.EscapeDataString(clientID) + "&";
            request += "redirect_uri=" + serverUrl + "&";
            request += "response_type=" + "code" + "&";
            request += "scope=" + Uri.EscapeDataString("https://www.googleapis.com/auth/spreadsheets") + "&";
            request += "access_type=" + "offline" + "&";
            request += "prompt=" + "consent" + "&";

            Application.OpenURL(request);
        }

        public static void GetAuthComplete(string authToken)
        {
            var serverUrl = $"http://127.0.0.1:{DailyG2UManager.Config.PORT}";

            WWWForm f = new WWWForm();
            f.AddField("code", authToken);
            f.AddField("client_id", DailyG2UManager.Config.CLIENT_ID);
            f.AddField("client_secret", DailyG2UManager.Config.CLIENT_SECRET);
            f.AddField("redirect_uri", serverUrl);
            f.AddField("grant_type", "authorization_code");
            f.AddField("scope", "");
            GetToken(f);
        }
        private static async void GetToken(WWWForm f)
        {
            using (UnityWebRequest request = UnityWebRequest.Post("https://accounts.google.com/o/oauth2/token", f))
            {
                await request.SendWebRequest();

                Debug.Log(request.downloadHandler.text);
                if (request.downloadHandler.text.Contains("error"))
                {
                    CompleteEvent?.Invoke("驗證錯誤");
                    return;
                }

                DailyG2UManager.Config.response = JsonUtility.FromJson<GoogleSheetConfig.GoogleDataResponse>(request.downloadHandler.text);
                DailyG2UManager.Config.response.nextRefreshTime = DateTime.Now.AddSeconds(DailyG2UManager.Config.response.expires_in);
                EditorUtility.SetDirty(DailyG2UManager.Config);
                AssetDatabase.SaveAssets();
                DailyG2UManager.Config.Save();
                CompleteEvent?.Invoke("已獲取GOOGLE存取憑證");
                Debug.Log("已獲取GOOGLE存取憑證");
            }
        }

        public static async Task CheckForRefreshOfToken()
        {
            if (DateTime.Now > DailyG2UManager.Config.response.nextRefreshTime)
            {
                Debug.Log("Refreshing Token");

                WWWForm f = new WWWForm();
                f.AddField("client_id", DailyG2UManager.Config.CLIENT_ID);
                f.AddField("client_secret", DailyG2UManager.Config.CLIENT_SECRET);
                f.AddField("refresh_token", DailyG2UManager.Config.response.refresh_token);
                f.AddField("grant_type", "refresh_token");
                f.AddField("scope", "");

                using (UnityWebRequest request = UnityWebRequest.Post("https://www.googleapis.com/oauth2/v4/token", f))
                {
                    await request.SendWebRequest();

                    GoogleSheetConfig.GoogleDataResponse newGdr = JsonUtility.FromJson<GoogleSheetConfig.GoogleDataResponse>(request.downloadHandler.text);
                    DailyG2UManager.Config.response.access_token = newGdr.access_token;
                    DailyG2UManager.Config.response.nextRefreshTime = DateTime.Now.AddSeconds(newGdr.expires_in);

                    EditorUtility.SetDirty(DailyG2UManager.Config);
                    AssetDatabase.SaveAssets();
                }

            }
        }

        private static async void Listener()
        {
            while (_isListening)
            {
                var context = await _httpListener.GetContextAsync();
                await Task.Factory.StartNew(() => ProcessRequest(context));
            }
            _httpListener.Close();

            GetAuthComplete(DailyG2UManager.Config.ACCESS_TOKEN);
        }

        private static void ProcessRequest(HttpListenerContext context)
        {
            var token = context.Request.QueryString["code"];
            DailyG2UManager.Config.ACCESS_TOKEN = token;
            _isListening = false;

            var message = Encoding.UTF8.GetBytes("<h1>您可以關閉此頁</h1>");
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.ContentType = "text/html";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.ContentLength64 = message.Length;
            context.Response.OutputStream.Write(message, 0, message.Length);
            context.Response.OutputStream.Flush();
            context.Response.Close();
        }

    }
}