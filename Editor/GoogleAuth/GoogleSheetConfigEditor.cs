using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UIElements.Button;

namespace Daily.G2U
{
    public class GoogleSheetConfigEditor : EditorWindow
    {
//        public static GoogleSheetConfigEditor instance;
        public GoogleSheetConfig config;
        private HelpBox _currentState;

        [MenuItem("Tools/GoogleSheetToUnity/Config", false, priority: 0)]
        private static void ShowWindow()
        {
            var instance = GetWindow<GoogleSheetConfigEditor>();
            instance.titleContent = new GUIContent("G2U Config");
            instance.config = DailyG2UManager.Config;
        }

        private void OnEnable()
        {
            GoogleAuth2Utility.CompleteEvent += OnAuthEnd;
        }

        private void OnDisable()
        {
            GoogleAuth2Utility.CompleteEvent -= OnAuthEnd;
        }

        private void CreateGUI()
        {
            var tryLoad = new Button(Load)
            {
                text = "嘗試載入"
            };

            var step1 = new Label("第一步");

            var openGoogleApiBt = new Button(GoogleAuth2Utility.OpenUrlGoogleAPI)
            {
                text = "開啟Google憑證管理"
            };

            var startAuthBt = new Button(() => GoogleAuth2Utility.StartAuth2(DailyG2UManager.Config.CLIENT_ID))
            {
                text = "開始驗證"
            };

            var step2 = new Label("第二步");

            var idTextField = new TextField("客戶端編號")
            {
                value = DailyG2UManager.Config.CLIENT_ID
            };
            idTextField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                DailyG2UManager.Config.CLIENT_ID = evt.newValue;
            });

            var secretTextField = new TextField("客戶端密碼")
            {
                value = DailyG2UManager.Config.CLIENT_SECRET,
                isPasswordField = true
            };
            secretTextField.RegisterCallback<ChangeEvent<string>>(evt =>
            {
                DailyG2UManager.Config.CLIENT_SECRET = evt.newValue;
            });

            var portField = new IntegerField("連接埠")
            {
                value = DailyG2UManager.Config.PORT
            };
            portField.RegisterCallback<ChangeEvent<int>>(evt =>
            {
                DailyG2UManager.Config.PORT = evt.newValue;
            });

            _currentState = new HelpBox();
            if (string.IsNullOrEmpty(DailyG2UManager.Config.response?.access_token))
            {
                _currentState.messageType = HelpBoxMessageType.Error;
                _currentState.text = "沒有憑證";
            }
            else
            {
                OnAuthEnd(null);
            }
            
            if (CheckSaveData())
                rootVisualElement.Add(tryLoad);
            rootVisualElement.Add(step1);
            rootVisualElement.Add(openGoogleApiBt);
            rootVisualElement.Add(idTextField);
            rootVisualElement.Add(secretTextField);
            rootVisualElement.Add(portField);
            rootVisualElement.Add(step2);
            rootVisualElement.Add(startAuthBt);
            rootVisualElement.Add(_currentState);
        }
        private bool CheckSaveData()
        {
            var token = "";
            token = EditorPrefs.GetString("G2U_ACCESS_TOKEN", token);
            return !string.IsNullOrEmpty(token);
        }
        private void Load()
        {
            DailyG2UManager.Config.Load();
            rootVisualElement.Clear();
            CreateGUI();
        }

        private void OnAuthEnd(string obj)
        {
            _currentState.messageType = HelpBoxMessageType.Info;
            _currentState.text = "已完成認證";
        }
    }
}