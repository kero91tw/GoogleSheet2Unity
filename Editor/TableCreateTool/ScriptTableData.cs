﻿using System.Collections.Generic;
using System.Text;
namespace Daily.G2U.TableCreateTool
{
    
    public class ScriptTableData
    {
        public string ClassName;
        public List<FiledData> FiledList = new List<FiledData>();

        public string GetTableTxt()
        {
            var sb = new StringBuilder();
            foreach (var item in FiledList)
            {
                sb.Append($"{item.Type} {item.FiledName} \t");
            }
            return sb.ToString();
        }
    }
    
    public class FiledData
    {
        public string FiledName;
        public FiledType Type;
        public bool IsArray;
    }
    
    public enum FiledType
    {
        Unknown,
        String,
        Int,
        Float,
        Bool,
        Enum,
        SubClass,
    }
}