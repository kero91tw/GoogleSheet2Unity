﻿
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
namespace Daily.G2U.TableViewer
{
    public class TableViewer : EditorWindow
    {
        private Dictionary<Type,  List<ScriptableObject>> keymap = new Dictionary<Type, List<ScriptableObject>>();
        private string _nowSearchKeyword;
        
        [MenuItem("Tools/GoogleSheetToUnity/TableViewer")]
        private static void ShowWindow()
        {
            var window = GetWindow<TableViewer>();
            window.titleContent = new GUIContent("TableViewer");
            window.Show();
        }
        private void OnEnable()
        {
            var tables = TableUpdateHelper.GetAllTable();
            foreach (var item in tables)
            {
                if (keymap.TryGetValue(item.GetType(), out var group))
                {
                    group.Add(item);
                }
                else
                {
                    keymap.Add(item.GetType(),new List<ScriptableObject>{item});
                }
            }
        }
        
        public void CreateGUI()
        {
            var updateAll = new ToolbarButton(UpdateAll);
            updateAll.text = "更新列表";
            var search = new ToolbarSearchField();
            var toolbar = new Toolbar();
            toolbar.Add(search);
            toolbar.Add(updateAll);

            var group1 = new VisualElement();
            group1.style.width = new StyleLength(Length.Percent(30));
            var group2 = new VisualElement();
            group2.style.width = new StyleLength(Length.Percent(100));
            
            var root = new VisualElement();
            rootVisualElement.Add(toolbar);
            rootVisualElement.Add(root);
            root.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
            root.Add(group1);
            root.Add(group2);
            
            UpdateList(group1,group2);
            search.RegisterCallback<InputEvent>(evt =>
            {
                _nowSearchKeyword = evt.newData;
                UpdateList(group1,group2);
            });
        } 
        
        private void UpdateList(VisualElement group1, VisualElement group2)
        {
            group1.Clear();
            foreach (var tableGroup in keymap)
            {
                group1.Add(new Label(tableGroup.Key.Name));
                foreach (var table in tableGroup.Value)
                {
                    if (!string.IsNullOrEmpty(_nowSearchKeyword) && !table.name.ToUpper().Contains(_nowSearchKeyword.ToUpper()))
                    {
                        continue;
                    }
                
                    var bt = new Button(()=> ShowInfo(group2,table))
                    {
                        text = table.name
                    };
                    group1.Add(bt);
                }
            }
        }
        
        private void ShowInfo(VisualElement root, ScriptableObject table)
        {
            root.Clear();
            var s = new InspectorElement(table);
            root.Add(s);
            EditorGUIUtility.PingObject(table);
        }

        public  void UpdateAll()
        {
            foreach (var item in keymap)
            {
                foreach (var item2 in item.Value)
                {
                    G2UDataEditor.Update(item2);
                }
            }
        }
    }
}